A user's password can be generated like so from Django shell:

	>>> from django.contrib.auth.models import User
	>>> u = User()
	>>> u.set_password('newpass')
	>>> u.password
	'sha1$e2fd5$96edae9adc8870fd87a65c051e7fdace6226b5a8'
