$.sentiment = {
    data:[],
    updateSentimentGraph: function(chartId) {
        nv.addGraph(function() {
            var chart = nv.models.lineWithFocusChart();
            //var chart = nv.models.lineChart();

            chart.xAxis.tickFormat(function(d) {
                return d3.time.format('%d/%m/%y %H:%M')(new Date(d));
            });
            chart.x2Axis.tickFormat(function(d) {
                return d3.time.format('%d/%m/%y %H:%M')(new Date(d));
            });

            chart.yAxis.tickFormat(d3.format(',.02f'));
            chart.y2Axis.tickFormat(d3.format(',.2f'));

            d3.select(chartId + ' svg').remove();
            d3.select(chartId).append('svg:svg').attr("id", function() { return "sentiment_graph"; });

            d3.select(chartId + ' svg')
            .datum($.sentiment.data)
            .transition().duration(500)
            .call(chart);

            nv.utils.windowResize(chart.update);

            return chart;
        });
    }
};
