from django.db import models

class Keyword(models.Model):
    source = models.CharField(max_length=32)
    value = models.CharField(max_length=64)

class UserKeyword(models.Model):
    count = models.IntegerField()
    value = models.CharField(max_length=64, unique=True)

