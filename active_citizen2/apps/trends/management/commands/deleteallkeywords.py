#!/usr/bin/env python

from django.core.management.base import BaseCommand, CommandError
from twittertrends.models import Keyword

class Command(BaseCommand):
    args = '<source>'
    help = 'Delete all keywords from specified source'

    def handle(self, *args, **options):
        source = args[0]
        try:
            Keyword.objects.filter(source=source).delete()
        except Keyword.DoesNotExist:
            pass
