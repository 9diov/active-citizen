from django.views.generic import TemplateView, View
from django.template.response import TemplateResponse
from trends.models import Keyword
from account.models import SearchString
from django import http
import logging
logger = logging.getLogger('to_console')

class HomeView(View):
    def get(self, request, *args, **kwargs):
            return TemplateResponse(
                request = request,
                template = 'homepage.html',
                context = { 'params': kwargs },
                **kwargs
            )

class DashboardView(TemplateView):
    template_name = "dashboard.html"

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        try:
            context['google_keywords'] = Keyword.objects.filter(source="google")
            context['twitter_keywords'] = Keyword.objects.filter(source="twitter")
            context['user_keywords'] = SearchString.objects.values("search_string").distinct()[:10]
        except Keyword.DoesNotExist:
            logger.debug("Failed to get keywords from database")
        return context

class GraphsView(TemplateView):
    template_name = "graphs.html"
